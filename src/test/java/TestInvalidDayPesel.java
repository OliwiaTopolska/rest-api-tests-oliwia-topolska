import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static org.testng.Assert.assertEquals;

public class TestInvalidDayPesel {
    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateInvalidPesel_InvalidDay() {
        Response response = get("/Pesel?pesel=90063102119");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVD");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid day.");

    }


    @Test
    public void testGetRequest_InvalidDay() {
        Response response = get("/Pesel?pesel=90063102119");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVD");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid day.");
    }


}

