import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;


public class TestInvalidPeselLengthINVL {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }
        @Test
    public void testGetRequest_ResponseErrorCode_12Characters() {
        Response response = get("/Pesel?pesel=790815716351");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVL");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid length. Pesel should have exactly 11 digits.");
    }

    @Test
    public void testGetRequest_ResponseErrorCode_10Characters() {
        Response response = get("/Pesel?pesel=4810077984");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVL");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid length. Pesel should have exactly 11 digits.");
    }
}
