import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;


public class TestInvalidPeselCheckSumINVC_bdd {
    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }



    @Test
    public void testGetRequest_ResponseElements_CheckSum() {

        Response response = get("/Pesel?pesel=81052174854");
        String actualBody = response.getBody().asString();
        String expectedBody =("{\"pesel\":\"81052174854\",\"isValid\":false,\"birthDate\":\"1981-05-21T00:00:00\",\"sex\":" +
                "\"Male\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"}]}");

        Assert.assertEquals(actualBody,expectedBody);
    }

    }

