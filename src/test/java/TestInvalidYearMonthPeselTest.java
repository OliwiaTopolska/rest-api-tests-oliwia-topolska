import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;


public class TestInvalidYearMonthPeselTest {
    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateInvalidPesel_InvalidYearInvalidMonth() {
        Response response = get("/Pesel?pesel=90001512147");
        String actualBody = response.getBody().asString();
        String expectedBody = ("{\"pesel\":\"90001512147\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Female\",\"errors\":" +
                "[{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}");

        Assert.assertEquals(actualBody, expectedBody);
    }

}
