import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class TestInvalidCharacterPesel {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateInvalidPesel_InvalidCharacter_Letter(){
        Response response = get("/Pesel?pesel=491115805E");
        String actualBody = response.getBody().asString();
        String expectedBody = ("{\"pesel\":\"491115805E\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":" +
                "[{\"errorCode\":\"NBRQ\",\"errorMessage\":\"Invalid characters. Pesel should be a number.\"}]}");

        Assert.assertEquals(actualBody,expectedBody);
    }

    @Test
    public void validateInvalidPesel_InvalidCharacter_Char(){
        Response response = get("/Pesel?pesel=491/115801");
        String actualBody = response.getBody().asString();
        String expectedBody = ("{\"pesel\":\"491/115801\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":" +
                "[{\"errorCode\":\"NBRQ\",\"errorMessage\":\"Invalid characters. Pesel should be a number.\"}]}");

        Assert.assertEquals(actualBody,expectedBody);
    }
}
