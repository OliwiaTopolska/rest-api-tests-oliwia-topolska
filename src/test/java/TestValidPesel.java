import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;


public class TestValidPesel {
    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateValidPesel_IsValid() {
        Response response = get("/Pesel?pesel=28122902767");

        boolean actualIsValid = response.getBody().path("isValid");
        Assert.assertTrue(actualIsValid);

    }

    @Test
    public void testGetRequest_responseElements_IsValidBirthDate() {
        Response response = get("Pesel?pesel=28122902767");
        Assert.assertTrue(response.path("isValid"));

        String birthdate = response.path("birthDate");
        Assert.assertEquals(birthdate, "1928-12-29T00:00:00");
    }

    @Test
    public void testGetRequest_responseElements_CheckSexF() {
        Response response = get("/Pesel?pesel=48100779844");

        String actualResponse = response.path("sex");
        Assert.assertEquals(actualResponse, "Female");
    }

    @Test
    public void testGetRequest_responseElements_CheckSexM() {
        Response response = get("/Pesel?pesel=00251395171");

        String actualResponse = response.path("sex");
        Assert.assertEquals(actualResponse, "Male");
    }

}
