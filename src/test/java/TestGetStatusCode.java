import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;


public class TestGetStatusCode {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

        @Test
    public void testGetRequest_ResponseStatusCodeOk() {
        Response response = get("Pesel?pesel=87090530912");

        Assert.assertEquals(response.statusCode(),200);
    }

    @Test
    public void testGetRequest_ResponseStatusCodeNotFound() {
        Response response = get("/Pesel");
        Assert.assertEquals(response.statusCode(),400);
    }

}
